#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "utils.h"

using namespace ALTechTest;
using namespace embree;

TEST_CASE("ClosestPointToTriangle", "[closestPointToTriangle]")
{
    Triangle t{Vec3fa(0.0f, 1.0f, 0.0f, 1.0f), Vec3fa(1.0f, 0.0f, 0.0f, 1.0f),
               Vec3fa(-1.0f, 0.0f, 0.0f, 1.0f)};

    std::vector< std::pair< Vec3fa, Vec3fa > > queryPointAndResult;

    float inf = std::numeric_limits< float >::infinity();
    // Vertex point cases
    queryPointAndResult.push_back(
        std::make_pair(Vec3fa(0.0f, 2.0f, 0.0f), Vec3fa(0.0f, 1.0f, 0.0f)));
    queryPointAndResult.push_back(
        std::make_pair(Vec3fa(2.0f, 0.0f, 0.0f), Vec3fa(1.0f, 0.0f, 0.0f)));
    queryPointAndResult.push_back(
        std::make_pair(Vec3fa(-2.0f, 0.0f, 0.0f), Vec3fa(-1.0f, 0.0f, 0.0f)));

    // Vertex edge cases
    queryPointAndResult.push_back(
        std::make_pair(Vec3fa(1.5f, 2.0f, 1.0f), Vec3fa(0.25f, 0.75f, 0.0f)));
    queryPointAndResult.push_back(std::make_pair(Vec3fa(-1.5f, 2.0f, -1.0f),
                                                 Vec3fa(-0.25f, 0.75f, 0.0f)));
    queryPointAndResult.push_back(
        std::make_pair(Vec3fa(0.0f, -2.0f, 0.0f), Vec3fa(0.0f, 0.0f, 0.0f)));
    // Face case
    queryPointAndResult.push_back(
        std::make_pair(Vec3fa(0.0f, 0.5f, -2.5f), Vec3fa(0.0f, 0.5f, 0.0f)));
    queryPointAndResult.push_back(
        std::make_pair(Vec3fa(0.0f, 0.5f, 20.0f), Vec3fa(0.0f, 0.5f, 0.0f)));

    for (auto p : queryPointAndResult)
    {
        auto res = t.ClosestPoint(p.first);
        REQUIRE(res.x == Approx(p.second.x));
        REQUIRE(res.y == Approx(p.second.y));
        REQUIRE(res.z == Approx(p.second.z));
    }
}

class ClosestPointToMeshFixture
{
   public:
    Mesh m_mesh;
    std::unique_ptr< ClosestPoint > m_cp;

   public:
    ClosestPointToMeshFixture()
    {
        m_mesh.load("cornell_box.obj");
        m_cp = std::make_unique< ClosestPoint >(&m_mesh);
    }
};

TEST_CASE_METHOD(ClosestPointToMeshFixture, "ClosestPointToMesh",
                 "[closestPointToMesh]")
{
    std::vector< std::pair< Vec3fa, Vec3fa > > queryPointAndResult;

    // Vertex point cases
    queryPointAndResult.push_back(std::make_pair(
        Vec3fa(689.0f, 715.0f, 740.0f), Vec3fa(556.0f, 548.8f, 559.2f)));

    // Vertex edge cases
    queryPointAndResult.push_back(std::make_pair(Vec3fa(-60.0f, -60.0f, 196.0f),
                                                 Vec3fa(0.0f, 0.0f, 196.0f)));
    ;
    // Face case
    queryPointAndResult.push_back(std::make_pair(Vec3fa(-60.0f, 265.0f, 105.0f),
                                                 Vec3fa(0.0f, 265.0f, 105.0f)));
    queryPointAndResult.push_back(std::make_pair(
        Vec3fa(465.0f, 280.0f, 160.0f), Vec3fa(553.511f, 279.484f, 160.507f)));

    for (auto p : queryPointAndResult)
    {
        auto res = (*m_cp)(p.first, 100000);
        REQUIRE(res.x == Approx(p.second.x));
        REQUIRE(res.y == Approx(p.second.y));
        REQUIRE(res.z == Approx(p.second.z));
    }
}