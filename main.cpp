#include <embree3/rtcore.h>
#include <tiny_obj_loader.h>
#include <algorithm>
#include <chrono>
#include <numeric>
#include <random>

#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/tbb.h>
#include "embree-3.1/common/math/vec3.h"

#include "utils.h"

using namespace embree;
using namespace ALTechTest;

void ClosestPointToMeshDistanceQuery(std::vector< Vec3fa > *points, Mesh *mesh,
                                     std::vector< Vec3fa > *closestPoints,
                                     float maxDist)
{
    ClosestPoint cp(mesh);
    tbb::task_scheduler_init init();
    auto start = std::chrono::system_clock::now();

    tbb::parallel_for(tbb::blocked_range< size_t >(0, (*points).size(), 100),
                      [=](const tbb::blocked_range< size_t > &r) {
                          for (size_t i = r.begin(); i != r.end(); ++i)
                          {
                              Vec3fa p = (*points)[i];
                              (*closestPoints)[i] = cp(p, maxDist);
                          }
                      });
    auto end = std::chrono::system_clock::now();

    std::chrono::duration< double > elapsed_seconds = end - start;

    std::cout << "Elapsed Time in seconds: " << elapsed_seconds.count()
              << std::endl;
}

void ClosestPointToMeshDistanceQueryBVH(std::vector< Vec3fa > *points,
                                        BVHNode *bvh,
                                        std::vector< Vec3fa > *closestPoints,
                                        float maxDist, float epsilon)
{
    ClosestPointBVH cp(bvh);
    tbb::task_scheduler_init init(1);
    auto start = std::chrono::system_clock::now();

    tbb::parallel_for(tbb::blocked_range< size_t >(0, (*points).size()),
                      [=](const tbb::blocked_range< size_t > &r) {
                          for (size_t i = r.begin(); i != r.end(); ++i)
                          {
                              Vec3fa p = (*points)[i];
                              (*closestPoints)[i] = cp(p);
                          }
                      });
    auto end = std::chrono::system_clock::now();

    std::chrono::duration< double > elapsed_seconds = end - start;

    std::cout << "Elapsed Time in seconds: " << elapsed_seconds.count()
              << std::endl;
}

std::vector< std::string > split(const std::string &s, char delimiter)
{
    std::vector< std::string > tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter))
    {
        tokens.push_back(token);
    }
    return tokens;
}

int main(int argc, char **argv)
{
    std::vector< Vec3fa > points, closestPoints;
    // Default files
    std::string model("cornell_box.obj");
    std::string pointsCSV = "cornell_box.csv";
    float maxDistance = std::numeric_limits< float >::infinity();
    bool useBVH = false;
    bool loadPointsFromFile = false;
    if (argc > 0)
    {
        for (int i = 1; i < argc; ++i)
        {
            if (strcmp(argv[i], "--mesh") == 0)
            {
                if (argc > i)
                {
                    model = argv[i + 1];
                }
            }
            if (strcmp(argv[i], "--points") == 0)
            {
                if (argc > i)
                {
                    pointsCSV = argv[i + 1];
                    loadPointsFromFile = true;
                }
            }
            if (strcmp(argv[i], "--bvh") == 0)
            {
                if (argc > i)
                {
                    std::string result = argv[i + 1];
                    if (result == "y")
                    {
                        useBVH = true;
                    }
                }
            }
            if (strcmp(argv[i], "--maxDistance") == 0)
            {
                if (argc > i)
                {
                    std::string result = argv[i + 1];

                    float num = std::stof(argv[i + 1]);
                    if (num >= 0.0f)
                    {
                        maxDistance = num;
                        std::cout << "Max Distance is " << maxDistance
                                  << std::endl;
                    }
                }
            }
            if (strcmp(argv[i], "--help") == 0)
            {
                std::cout << "Arguments are:" << std::endl;
                std::cout << "--mesh /path/to/file (OPTIONAL)" << std::endl;
                std::cout << "--points /path/to/file (OPTIONAL)" << std::endl;
                std::cout << "--bvh y if yes" << std::endl;
                std::cout << "--maxDistance ### some float number" << std::endl;
                return 0;
            }
        }
    }

    // Load mesh
    Mesh mesh;
    mesh.load(model);
    int maxPoints = 0;

    if (loadPointsFromFile)
    {
        // Load points from the CSV file
        std::ifstream file(pointsCSV);
        std::string line;
        while (getline(file, line))
        {
            maxPoints++;
            auto point = split(line, ',');
            Vec3fa v{stof(point[0]), stof(point[1]), stof(point[2])};
            std::cout << v << std::endl;
            points.push_back(v);
        }
    }
    else
    {
        // Generate the random points
        maxPoints = 10000;

        std::random_device rd{};
        std::mt19937 engine{rd()};
        std::uniform_real_distribution< float > dist;

        for (int i = 0; i < maxPoints; ++i)
        {
            Vec3fa p(dist(engine) * 300.0f, dist(engine) * 300.0f,
                     dist(engine) * 300.0f);
            points.push_back(p);
        }
    }
    closestPoints.resize(points.size());
    if (useBVH)
    {
        auto start = std::chrono::system_clock::now();
        std::cout << "Loading scene and building BVH..." << std::endl;

        BVHNode bvh((Shape **)(mesh.m_triangles.data()),
                    mesh.m_triangles.size());

        auto end = std::chrono::system_clock::now();

        std::chrono::duration< double > elapsed_seconds = end - start;

        std::cout << "Time to build BVH: " << elapsed_seconds.count()
                  << std::endl;

        ClosestPointToMeshDistanceQueryBVH(&points, &bvh, &closestPoints,
                                           maxDistance, 1.0f);
    }
    else
    {
        ClosestPointToMeshDistanceQuery(&points, &mesh, &closestPoints,
                                        maxDistance * maxDistance);
    }

    for (int i = 0; i < maxPoints; ++i)
    {
        std::cout << "Closest point to " << points[i] << " is "
                  << closestPoints[i] << std::endl;
    }

    return 0;
}
