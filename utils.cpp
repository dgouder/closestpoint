#include "utils.h"

#include "tiny_obj_loader.h"

using namespace ALTechTest;
using namespace embree;

AABB::AABB(const Vec3fa &a, const Vec3fa &b) : m_min(a), m_max(b)
{
}

Vec3fa AABB::ClosestPoint(Vec3fa p) const
{
    Vec3fa q;
    for (int i = 0; i < 3; ++i)
    {
        float v = p[i];
        if (v < m_min[i])
        {
            v = m_min[i];
        }
        if (v > m_max[i])
        {
            v = m_max[i];
        }
        q[i] = v;
    }
    return q;
}

Vec3fa Triangle::ClosestPoint(Vec3fa p) const
{
    auto a = vertices[0];
    auto b = vertices[1];
    auto c = vertices[2];
    // Check if p is in the vertex region outside of A
    Vec3fa ab = b - a;
    Vec3fa ac = c - a;
    Vec3fa ap = p - a;

    float d1 = dot(ab, ap);
    float d2 = dot(ac, ap);
    if (d1 <= 0.0f && d2 <= 0.0f)
    {
        // the closest point is a
        return a;
    }

    Vec3fa bp = p - b;
    float d3 = dot(ab, bp);
    float d4 = dot(ac, bp);
    if (d3 >= 0.0f && d4 <= d3)
    {
        // the closest point is b
        return b;
    }

    // Check if p is in the edge region of AB
    float vc = d1 * d4 - d3 * d2;
    if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f)
    {
        float v = d1 / (d1 - d3);
        return a + v * ab;
    }

    // Check fi P in vertex region outside C
    Vec3fa cp = p - c;
    float d5 = dot(ab, cp);
    float d6 = dot(ac, cp);
    if (d6 >= 0.0f && d5 <= d6)
    {
        return c;
    }

    // Check if p is in edge region of AC, if so return the projection of p onto
    // ac
    float vb = d5 * d2 - d1 * d6;
    if (vb <= 0.0f && d2 > 0.0f && d6 <= 0.0f)
    {
        float w = d2 / (d2 - d6);
        return a + w * ac;
    }

    // Check if p is in the edge region of bc, if so return the projection of p
    // onto bc
    float va = d3 * d6 - d5 * d4;
    if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f)
    {
        float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
        return b + w * (c - b);
    }

    // p is inside the face region. compute q through its barycentric
    // coordinates
    float denom = 1.0f / (va + vb + vc);
    float v = vb * denom;
    float w = vc * denom;
    return a + ab * v + ac * w;
}

AABB Triangle::BoundingBox() const
{
    float v0x = vertices[0].x;
    float v0y = vertices[0].y;
    float v0z = vertices[0].z;

    float v1x = vertices[1].x;
    float v1y = vertices[1].y;
    float v1z = vertices[1].z;

    float v2x = vertices[2].x;
    float v2y = vertices[2].y;
    float v2z = vertices[2].z;

    // Get the maximum value of each component
    float xMax = v0x > v1x ? (v0x > v2x ? v0x : v2x) : (v1x > v2x ? v1x : v2x);
    float yMax = v0y > v1y ? (v0y > v2y ? v0y : v2y) : (v1y > v2y ? v1y : v2y);
    float zMax = v0z > v1z ? (v0z > v2z ? v0z : v2z) : (v1z > v2z ? v1z : v2z);

    // Get the minimum value of each component
    float xMin = v0x < v1x ? (v0x < v2x ? v0x : v2x) : (v1x < v2x ? v1x : v2x);
    float yMin = v0y < v1y ? (v0y < v2y ? v0y : v2y) : (v1y < v2y ? v1y : v2y);
    float zMin = v0z < v1z ? (v0z < v2z ? v0z : v2z) : (v1z < v2z ? v1z : v2z);

    return AABB(embree::Vec3fa(xMin, yMin, zMin),
                embree::Vec3fa(xMax, yMax, zMax));
}

// I know I should be ashamed for using rand()
double randZeroToOne()
{
    return rand() / (RAND_MAX + 1.);
}

int box_x_compare(const void *a, const void *b)
{
    Shape *ah = *(Shape **)a;
    Shape *bh = *(Shape **)b;
    ;

    if (ah->BoundingBox().m_min.x - bh->BoundingBox().m_min.x < 0.0)
        return -1;
    else
        return 1;
}

int box_y_compare(const void *a, const void *b)
{
    Shape *ah = *(Shape **)a;
    Shape *bh = *(Shape **)b;

    if (ah->BoundingBox().m_min.y - bh->BoundingBox().m_min.y < 0.0)
        return -1;
    else
        return 1;
}
int box_z_compare(const void *a, const void *b)
{
    Shape *ah = *(Shape **)a;
    Shape *bh = *(Shape **)b;

    if (ah->BoundingBox().m_min.z - bh->BoundingBox().m_min.z < 0.0)
        return -1;
    else
        return 1;
}

BVHNode::BVHNode(Shape **shapes, size_t objectCount)
{
    int axis = int(3 * randZeroToOne());
    if (axis == 0)
    {
        qsort(shapes, objectCount, sizeof(Shape *), box_x_compare);
    }
    else if (axis == 1)
    {
        qsort(shapes, objectCount, sizeof(Shape *), box_y_compare);
    }
    else
    {
        qsort(shapes, objectCount, sizeof(Shape *), box_z_compare);
    }

    if (objectCount == 1)
    {
        m_left = m_right = shapes[0];
    }
    else if (objectCount == 2)
    {
        m_left = shapes[0];
        m_right = shapes[1];
    }
    else
    {
        m_left = new BVHNode(shapes, objectCount / 2);
        m_right = new BVHNode(shapes + objectCount / 2,
                              objectCount - objectCount / 2);
    }

    m_box = BoundingBox();
}

embree::Vec3fa BVHNode::ClosestPoint(embree::Vec3fa p) const
{
    Vec3fa leftClosestPoint;
    Vec3fa rightClosestPoint;
    Vec3fa closestPoint;
    if (m_left->type() == 3 && m_right->type() == 3)
    {
        // Node is a BVH node

        // Determine closest AABB
        leftClosestPoint = ((BVHNode *)m_left)->m_box.ClosestPoint(p);
        rightClosestPoint = ((BVHNode *)m_right)->m_box.ClosestPoint(p);

        float leftDistance = length(p - leftClosestPoint);
        float rightDistance = length(p - rightClosestPoint);

        if (fabs(leftDistance - rightDistance) < 0.002f)
        {
            // Left and Right nodes have same bounding boxes, delve deeper to
            // find the one with the closer subnodes
            leftClosestPoint = m_left->ClosestPoint(p);
            rightClosestPoint = m_right->ClosestPoint(p);

            auto ll = p - leftClosestPoint;
            auto rl = p - rightClosestPoint;
            leftDistance = length(ll);
            rightDistance = length(rl);
        }
        if (leftDistance < rightDistance)
        {
            // Left node is closer
            return m_left->ClosestPoint(p);
        }
        if (rightDistance < leftDistance)
        {
            // Right node is closer
            return m_right->ClosestPoint(p);
        }
    }
    else if (m_left->type() == 2)
    {
        // Determine the closest triangle
        leftClosestPoint = ((Triangle *)m_left)->ClosestPoint(p);
        rightClosestPoint = ((Triangle *)m_right)->ClosestPoint(p);

        float leftDistance = length(p - leftClosestPoint);
        float rightDistance = length(p - rightClosestPoint);

        if (leftDistance < rightDistance)
        {
            return leftClosestPoint;
        }
        else
        {
            return rightClosestPoint;
        }
    }
}

void Mesh::load(std::string filepath)
{
    if (loaded)
    {
        for (auto tri : m_triangles)
        {
            delete tri;
        }
        loaded = false;
    }
    std::vector< tinyobj::shape_t > shapes;
    std::vector< tinyobj::material_t > materials;
    tinyobj::attrib_t attrib;

    std::string err;
    bool ret =
        tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filepath.c_str());

    if (!err.empty())
    {  // `err` may contain warning message.
        std::cerr << err << std::endl;
    }

    if (!ret)
    {
        exit(1);
    }

    for (const auto &shape : shapes)
    {
        // Create a new geometry for the triangle
        int f = 0;
        Triangle t;
        for (const auto index : shape.mesh.indices)
        {
            t.vertices[f] =
                embree::Vec3fa{attrib.vertices[3 * index.vertex_index + 0],
                               attrib.vertices[3 * index.vertex_index + 1],
                               attrib.vertices[3 * index.vertex_index + 2]};
            f++;
            if (f == 3)
            {
                m_triangles.push_back(new Triangle(t));
                f = 0;
            }
        }
    }

    loaded = true;
}

Mesh::~Mesh()
{
    if (loaded)
    {
        for (auto tri : m_triangles)
        {
            delete tri;
        }
    }
}

ClosestPoint::ClosestPoint(Mesh *mesh)
{
    m_mesh = mesh;
}

embree::Vec3fa ClosestPoint::operator()(const embree::Vec3fa &queryPoint,
                                        float maxDist) const
{
    float minDistance = std::numeric_limits< float >::infinity();
    Vec3fa closestPoint(minDistance, minDistance, minDistance);

    for (auto t : (m_mesh->m_triangles))
    {
        auto potentialClosestPoint = t->ClosestPoint(queryPoint);
        float l = sqr_length(queryPoint - potentialClosestPoint);
        if (l < minDistance && l < maxDist)
        {
            closestPoint = potentialClosestPoint;
            minDistance = l;
        }
    }

    return closestPoint;
}

ClosestPointBVH::ClosestPointBVH(BVHNode *bvh)
{
    m_bvh = bvh;
}

embree::Vec3fa ClosestPointBVH::operator()(
    const embree::Vec3fa &queryPoint) const
{
    return m_bvh->ClosestPoint(queryPoint);
}