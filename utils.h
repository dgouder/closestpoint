#pragma once

#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <unordered_map>

#include "embree-3.1/common/math/vec3.h"

namespace ALTechTest
{
class AABB;
class Shape
{
    // Shape public functions
   public:
    virtual embree::Vec3fa ClosestPoint(embree::Vec3fa p) const = 0;
    virtual AABB BoundingBox() const = 0;
    virtual int type() const = 0;
};

class AABB : public Shape
{
    // AABB public members
   public:
    embree::Vec3fa m_min;
    embree::Vec3fa m_max;
    // AABB public functions
   public:
    AABB() = default;
    AABB(const embree::Vec3fa &a, const embree::Vec3fa &b);

    embree::Vec3fa ClosestPoint(embree::Vec3fa p) const override;

    AABB BoundingBox() const override
    {
        return (*this);
    }

    int type() const override
    {
        return 1;
    }
};

class Triangle : public Shape
{
    // Triangle public members
   public:
    embree::Vec3fa vertices[3];

    // Triangle public functions
   public:
    Triangle() = default;
    Triangle(embree::Vec3fa v0, embree::Vec3fa v1, embree::Vec3fa v2)
    {
        vertices[0] = v0;
        vertices[1] = v1;
        vertices[2] = v2;
    }

    embree::Vec3fa ClosestPoint(embree::Vec3fa p) const override;
    AABB BoundingBox() const override;

    int type() const override
    {
        return 2;
    }
};

// BVH Node class - currently broken
class BVHNode : public Shape
{
    // BVHNode public members
   public:
    Shape *m_left, *m_right;
    AABB m_box;

    // BVHNode public functions
   public:
    BVHNode(Shape **objects, size_t objectsCount);

    embree::Vec3fa ClosestPoint(embree::Vec3fa p) const override;

    AABB BoundingBox() const override
    {
        // Builds a bounding box around its children
        AABB leftBox = m_left->BoundingBox();
        AABB rightBox = m_right->BoundingBox();
        embree::Vec3fa min(std::min(leftBox.m_min.x, rightBox.m_min.x),
                           std::min(leftBox.m_min.y, rightBox.m_min.y),
                           std::min(leftBox.m_min.z, rightBox.m_min.z));
        embree::Vec3fa max(std::max(leftBox.m_max.x, rightBox.m_max.x),
                           std::max(leftBox.m_max.y, rightBox.m_max.y),
                           std::max(leftBox.m_max.z, rightBox.m_max.z));

        return AABB{min, max};
    }

    int type() const override
    {
        return 3;
    }
};

class Mesh
{
   public:
    std::vector< Triangle * > m_triangles;
    void load(std::string filepath);
    bool loaded = false;

    ~Mesh();
};

// Closest Point class that uses a brute force method
class ClosestPoint
{
   private:
    Mesh *m_mesh;

   public:
    ClosestPoint(Mesh *mesh);
    embree::Vec3fa operator()(const embree::Vec3fa &queryPoint,
                              float maxDist) const;
};

// Closest point class for BVH - currently broken
class ClosestPointBVH
{
   private:
    BVHNode *m_bvh;

   public:
    ClosestPointBVH() = delete;
    ClosestPointBVH(BVHNode *mesh);
    embree::Vec3fa operator()(const embree::Vec3fa &queryPoint) const;
};

}  // namespace ALTechTest
