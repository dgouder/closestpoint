# Closest Point to Mesh

Documentation that describes the code and my approach to tackling this problem.

# Building

To build, execute the build.sh script. I've tested this on Ubuntu, Windows and Mac.

# Application overview

The application and unit tests are compiled in the bin/ folder.
 
The application has 3 optional arguments

--mesh path/to/file :
The path to the file containing the obj file. If omitted cornell_box.obj in the models/ folder is loaded.

--points path/to/file :
The path to the file that contains a list of points to be tested. If omitted 1000 random points are generated.

--bvh y/n :
If 'y', the BVH is used but it has issues. If n or omitted, the brute force approach is used.

--maxDistance : 
The maximum distance for the search query.

Example run: ./closestpoint --file cornell_box.obj --points cornell_box.csv --useBVH y

To run the tests just run ./unittests

Make sure to copy the obj files into the bin folder.

## Structure

There are 2 ClosestPointOnMesh classes: ClosestPointOnMesh which is the brute force triangle soup version, and ClosestPointOnMeshBVH (the broken BVH approach).

The Shape interface defines what shapes are can be tested for closest point distance queries. The AABB is only used for BVHs. The brute-force approach is only useful for triangles.

## Assumptions

Meshes must be triangulated and loaded from obj files.

## Unit tests

To run the unit tests just run ./unittests in the bin folder. I should probably write an almost equal comparison on the Vec3fa class instead of doing the almost equal comparison per element, as when if they fail, it's not very helpful! 
## Known Issues

BVH will not necessarily give the correct closest point.
BVH will crash with certain scenes.

# Approach

I'll discuss here my methodology as to how I approached the problem. I spent a few hours reading up on the problem - I was familiar with the Real-Time Collision Detection textbook by Christer Ericson, and delved into that. The relevant topics on the closest point to triangle and the closest point to an AABB were extremely informative. I still took the time to read the details rather than just typing out the provided pseudo-code, given that without an understanding, it would not be possible to identify any potential bugs.

My first test case was on a single triangle. I confirmed my results by using Maya, and the ClosestPointOnMesh node. I've included the Maya scene for reference.
Once I got this working and wrote some unit tests for it, and then worked on loading meshes and running the ClosestPointToMesh function against this - using no form acceleration structure, just brute-forcing it.
I again confirmed my result against this using the Maya scene.

The next thing I did was to parallelize against multiple points. I used TBB for the threading capabilities and I saw a linear scaling in performance.


# Performance metrics

The following table shows the performance of the brute force parallelized query on a 4 core i7 3.7 GHz Intel Processor. The scene used was the sponza.obj file provided in the models folder, with approximately 270k triangles.

| Points | Time(s) |
|--------|---------|
| 1      |  0.0089 |
| 100    | 0.138   |
| 1000   | 1.37    |
| 10000  | 13.3    |
| 100000 | 135     |

## Vtune Basic Hotspot test
This is the basic hotspot test result from Vtune

![Vtune Basic hotspots image](img/vtuneCPU01.png "Vtune Basic Hotspots")

![Vtune Basic hotspots image](img/vtuneCPU02.png "Vtune Basic Hotspots Bottum Up view")

As expected, the majority of the time is spent identifying the closest triangle and identifying the closest point on that triangle.

The BVH would most certainly decrease the amount of calls to the Triangle::ClosestPoint function so that would probably give a substantial performance improvement. Furthermore, if we were to use SSE instructions to compare for example, 4 triangles at once, this could be beneficial in both the bruteforce and the BVH approach. The call to sqrtf is also a concern. Thinking a bit more about this, I could avoid calling sqrtf because even though I need to know the distance between the 2 points, I don't really need the square root of the calculated vector.  So I switched the function to using the sqr_length function and this significantly improved performance.

Unfortunately for some reason, my Windows project is having issues so the timings I have cannot really be compared well with the with Linux ones. Running a Vtune basic hotspot test on the improved code based, resulted in an almost 2x speed gain ( I was mistakenly calculating the length twice so that was also an issue). For 100,000 points, it took approximately 75 seconds which is about a 1.8x speedup.

![Vtune Basic hotspots image](img/vtuneCPU03.png "Vtune Basic Hotspots Bottum Up view after improving code and running on Linux")

I also noticed that performance-wise, the non-improved version performed slightly better than the Windows version. I'll need to compare these 2 platforms. 

## Vtune Memory access test

![Vtune Memory Access image](img/vtuneMemAccess01.png "Vtune Memory Access")
![Vtune Memory Access image](img/vtuneMemAccess02.png "Vtune Memory Access Bottum Up view")

We can see also a lot of memory-bound latency issues, as there's a lot of thrashing going on. This requires a bit more though, restructuring how I store my triangles, maybe moving to a more structure of arrays approach and use SSE instrinsics.


### Embree attempt
One of the next things I tried was to use embree's BVH and traverse it (it does not prove the capabilities to run point distance tests but the raw BVH is available). I didn't have any issues building it since I had used it before, but when I starting looking at the generated BVH data, I realized that I had degenerate triangles in the leaves. I was quite confused by this and ran a few test cases (the code to access the BVH was from their own tutorial samples). I was surprised by this result and I wondered if it was caused by some sort of  SIMD layout padding. I did not wish to waste a lot of time on this and felt it would be better served to focus on other things so I abandoned this approach. Nevertheless I'm quite curious as to what's really going on so I posted on the embree forums and on Twitter, once I have this submitted I'm making a repro test case to confirm that nothing is wrong on my end and continue the discussion with Sven Woop from Intel's embree team.

I've removed the relevant code from here but I can link the commits that had that part.
### Native BVH

I used my ray tracer's BVH which was originally sourced from Peter Shirley's "Ray Tracing the Next Week". I modified the traversal function, but I wasn't getting the correct results I wanted. I have reason to believe that the construction of the BVH is not entirely correct.

From my preliminary investigation it seems that the issue arises if the point is inside the mesh -  if the closest point is located on the edges, it seem to work fine.

 I was struggling to debug this without proper visualization and did not have enough time to properly find out what is going on.


## Plans and improvements

I'll probably continue messing around with this project because it's a good case study for experimentation with things like SIMD and acceleration structures.

The brute force triangle soup approach is an attractive opportunity to try that and is probably the first thing I'll try once I submit (although I've used intrinsics in the past I'm not overtly confident with them).

After that I'll work on visualization. If I can display the mesh with the bounding boxes, generated by the BVH, I can better figure out what's going wrong with my traversal, and then I can implement
some form of Linear BVH.